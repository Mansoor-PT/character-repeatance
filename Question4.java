import java.util.HashMap;
import java.util.Scanner;


 /*input= "abcd_abcd_abcd"
 output="_ _"
 */
public class Question4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter String :");
        String s = in.nextLine();
        HashMap<Character , Integer > map = new HashMap<Character,Integer>(); 
        int max = 0;
        for(int i= 0 ; i<s.length() ; i++){
            if(map.containsKey(s.charAt(i))){
                map.put(s.charAt(i), (map.get(s.charAt(i)))+1);
            }
            else{
                map.put(s.charAt(i), 1);
            }
            if(map.get(s.charAt(i))>max){
                max=map.get(s.charAt(i));
            }
        }
        for(int i=0 ; i<s.length();i++){
        
            if((map.get(s.charAt(i))!=max)){
                System.out.print(s.charAt(i));
            }
        }
    }
}

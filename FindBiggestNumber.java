/*
input:143            input:11023                     input:887
output:314           output:11032                    output:Not possible
 */
package level;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author mansoor-pt2603
 */
public class QuestionSorting {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the string:");
        String str = sc.nextLine();
        char ch[] = str.toCharArray();
        int len = str.length();
        int i;
        for (i = len - 1; i > 0; i--) {
            if (ch[i] > ch[i-1]) {
                break;
            }
        }
        if (i == 0) {
            System.out.println("it is not possible:");
        } else {
            int x = ch[i - 1];
            int min = i;
            for (int j = i + 1; j < len; j++) {
                if (ch[j] > x && ch[j] < ch[min]) {
                    min = j;
                }
            }
            int m=i-1;
            char temp = ch[m];
            ch[m] = ch[min];
            ch[min] = temp;
            Arrays.sort(ch, i, len);
            System.out.println("output:");
            for (int j = 0; j < len; j++) {
                System.out.print(ch[j]);
            }

        }
    }
}
